package sample.UI;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.DBConfig.ConnectToDB;
import sample.Domain.Accommodation.Accommodation;
import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Service.AccommodationService;
import sample.Service.ClientService;
import sample.Service.CompanyService;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Controller {
    @FXML
    public TableView tableViewReservations;
    public TableColumn tableColumnId;
    public TableColumn tableColumnClient;
    public TableColumn tableColumnCI;
    public TableColumn tableColumnCO;
    public TableColumn tableColumnRoomRate;
    public TableColumn tableColumnPriceType;
    public TableColumn tableColumnRoomType;
    public TableColumn tableColumnBalance;
    public Button btnReservationDelete;
    public Button btnReservationAdd;
    public Button btnReservationUndo;
    public Button btnReservationRedo;
    public Button btnReservationClear;
    public TextField filterName;
    public ToggleGroup radioGroup;
    public DatePicker selectStartDate;
    public DatePicker selectEndDate;
    public TextField txtField;
    public TableColumn tableColumnCompany;
    public MenuBar menuBar;
    public TableColumn tableColumnRestaurant;

    private AccommodationService accommodationService;
    private ClientService clientService;
    private CompanyService companyService;
    private ObservableList<Accommodation> accommodations = FXCollections.observableArrayList();



    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     *
     * Initializes the table columns and sets up sorting and filtering.
     */
    @FXML
    private void initialize() throws IOException {

        // Instantiate DB config class, validator, repository and service
        ConnectToDB conn = new ConnectToDB();
        conn.connectToDB();

        accommodationService = conn.getAccommodationService();

        System.out.println("Reservations from DB:" + accommodationService.getAll());

        Platform.runLater(() -> {
            accommodations.addAll(accommodationService.getAll());
            tableViewReservations.setItems(accommodations);

            //  Wrap the ObservableList in a FilteredList (initially display all data).
            FilteredList<Accommodation> filteredData = new FilteredList<>(accommodations, p -> true);

            //  Set the filter Predicate whenever the filter changes.
            filterName.textProperty().addListener((observable, oldValue, newValue) -> {
                filteredData.setPredicate(reservation -> {
                    // If filter text is empty, display all reservations.
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }
                    // Compare first name and last name of every person with filter text.
                    String lowerCaseFilter = newValue.toLowerCase();
                    if (reservation.getClient().toLowerCase().contains(lowerCaseFilter)) {
                        return true; // Filter matches name of client.
                    }
                    return false; // Does not match.
                });
            });

            // Wrap the FilteredList in a SortedList.
            SortedList<Accommodation> sortedData = new SortedList<>(filteredData);

            // Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(tableViewReservations.comparatorProperty());

            // Add sorted (and filtered) data to the table.
            tableViewReservations.setItems(sortedData);
        });
    }


    public void btnReservationAddClick(ActionEvent actionEvent) {

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/accommodationAdd.fxml"));

            Scene scene = new Scene(fxmlLoader.load(), 650, 500);
            Stage stage = new Stage();
            stage.setTitle("Reservation add");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            AccommodationAddController controller =  fxmlLoader.getController();
            controller.setService(accommodationService, clientService, companyService );
            stage.showAndWait();
            accommodations.clear();
            accommodations.addAll(accommodationService.getAll());
        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new window: Reservation add.", e);
        }
    }


    public void editClient(TableColumn.CellEditEvent cellEditEvent) {
        Accommodation editedRes = (Accommodation) cellEditEvent.getRowValue();
        try {
            String newClient = (String)cellEditEvent.getNewValue();
            Accommodation res = new Accommodation(newClient, editedRes.getCompany(), editedRes.getCheckIn(), editedRes.getCheckOut(), editedRes.getRoomRate(), editedRes.getPriceType(), editedRes.getRoomType());
            res.setId(editedRes.getId());
            accommodationService.update(res);
            editedRes.setClient(newClient);
        } catch (RuntimeException | CustomAccommodationException rex) {
            AlertBox.showValidationError(rex.getMessage());
        }
        tableViewReservations.refresh();
    }


    public void btnReservationDeleteClick(ActionEvent actionEvent) {
        Accommodation selected = (Accommodation) tableViewReservations.getSelectionModel().getSelectedItem();
        if (selected != null) {
            try {
                accommodationService.remove(selected.getId());
                accommodations.clear();
                accommodations.addAll(accommodationService.getAll());
            } catch (RuntimeException rex) {
                AlertBox.showValidationError(rex.getMessage());
            }
        }
    }

    public void btnReservationUndoClick(ActionEvent actionEvent) throws Exception {
        accommodationService.undo();
        accommodations.clear();
        accommodations.addAll(accommodationService.getAll());
    }

    public void btnReservationRedoClick(ActionEvent actionEvent) throws Exception {
        accommodationService.redo();
        accommodations.clear();
        accommodations.addAll(accommodationService.getAll());
    }

    public void btnReservationClearClick(ActionEvent actionEvent) {
        accommodationService.clear();
        accommodations.clear();
        accommodations.addAll(accommodationService.getAll());
    }

    public void resBetweenDatesClick(ActionEvent actionEvent) throws CustomAccommodationException {
        LocalDate startDate = selectStartDate.getValue();
        LocalDate endDate = selectEndDate.getValue();
        Set<Accommodation> filteredData = new HashSet<>(accommodationService.getReservationsBetweenDates(startDate, endDate));
        List list = new ArrayList<>(filteredData);
        ObservableList<Accommodation> observableList = FXCollections.observableArrayList(list);

        tableViewReservations.setItems(observableList);

    }

    public void menuViewReportsClick(ActionEvent actionEvent) {
     try {
         Parent viewReportsParent = FXMLLoader.load(getClass().getResource("/viewReports.fxml"));
         Scene viewReportScene = new Scene(viewReportsParent);

         // Need to get the stage information from Main
         // MenuItem does not inherit from Node so this fails.
//                 Node node= (Node)actionEvent.getSource();
//                 Stage stage=(Stage) node.getScene().getWindow();

         // MenuItem: with fx:id="viewReports" we inject it directly into the Controller
         Stage stage = (Stage) menuBar.getScene().getWindow();
            stage.setTitle("View reports");
            stage.setScene(viewReportScene);
            stage.show();

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new window: View reports.", e);
        }

    }

    public void menuGoToAddClick(ActionEvent actionEvent) {
        try {
            Parent addReservationParent = FXMLLoader.load(getClass().getResource("/accommodationAdd.fxml"));
            Scene addReservationScene = new Scene(addReservationParent);
            Stage stage = (Stage) menuBar.getScene().getWindow();
            stage.setTitle("Accommodation add");
            stage.setScene(addReservationScene);
            stage.show();

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new window: Reservation add.", e);
        }

        }


    public void menuGoToViewClientsClick(ActionEvent actionEvent) {
        try {
            Parent addReservationParent = FXMLLoader.load(getClass().getResource("/tableviewClients.fxml"));
            Scene addReservationScene = new Scene(addReservationParent);
            Stage stage = (Stage) menuBar.getScene().getWindow();
            stage.setTitle("Clients view");
            stage.setScene(addReservationScene);
            stage.show();

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new window: Reservation add.", e);
        }

    }

    public void menuGoToViewCompaniesClick(ActionEvent actionEvent) {
        try {
            Parent addReservationParent = FXMLLoader.load(getClass().getResource("/tableviewCompanies.fxml"));
            Scene addReservationScene = new Scene(addReservationParent);
            Stage stage = (Stage) menuBar.getScene().getWindow();
            stage.setTitle("Clients view");
            stage.setScene(addReservationScene);
            stage.show();

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new window: Reservation add.", e);
        }

    }

    public void menuGoToViewOrdersClick(ActionEvent actionEvent) {
        try {
            Parent addReservationParent = FXMLLoader.load(getClass().getResource("/tableviewOrders.fxml"));
            Scene addReservationScene = new Scene(addReservationParent);
            Stage stage = (Stage) menuBar.getScene().getWindow();
            stage.setTitle("Orders view");
            stage.setScene(addReservationScene);
            stage.show();

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new window: Reservation add.", e);
        }

    }


    // Either we inject reservationService, either with sql query

    public void viewMaxValueClick(ActionEvent actionEvent) throws IOException, CustomAccommodationException {
        LocalDate startDate = selectStartDate.getValue();
        LocalDate endDate = selectEndDate.getValue();
        ConnectToDB conn = new ConnectToDB();
        conn.connectToDB();
        Double maxBalance = conn.getAccommodationService().getMaxBalance(startDate, endDate);
        txtField.setText(maxBalance.toString());

    }

    public void viewNoOfStaysClick(ActionEvent actionEvent) throws IOException, CustomAccommodationException {
        LocalDate startDate = selectStartDate.getValue();
        LocalDate endDate = selectEndDate.getValue();
        ConnectToDB conn = new ConnectToDB();
        conn.connectToDB();
        Integer stays = conn.getAccommodationService().getCountReservations(startDate, endDate);
        txtField.setText(stays.toString());
    }

    public void viewNofOfNightsClick(ActionEvent actionEvent) throws CustomAccommodationException, IOException {
        LocalDate startDate = selectStartDate.getValue();
        LocalDate endDate = selectEndDate.getValue();
        ConnectToDB conn = new ConnectToDB();
        conn.connectToDB();
        Long totalNights = conn.getAccommodationService().getNightsNumber(startDate, endDate);
        txtField.setText(totalNights.toString());
    }

    public void viewTotalRevenuesClick(ActionEvent actionEvent) throws IOException, CustomAccommodationException {
        LocalDate startDate = selectStartDate.getValue();
        LocalDate endDate = selectEndDate.getValue();
        ConnectToDB conn = new ConnectToDB();
        conn.connectToDB();
        Double totalrevenues = conn.getAccommodationService().getTotalrevenues(startDate, endDate);
        txtField.setText(totalrevenues.toString());
    }

}
