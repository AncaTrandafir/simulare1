package sample.UI;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.DBConfig.ConnectToDB;
import sample.Domain.Accommodation.Accommodation;
import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.Client.Client;
import sample.Service.AccommodationService;
import sample.Service.ClientService;
import sample.Service.CompanyService;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class tableviewClients {
    @FXML
    public TableView tableViewClients;
    public TableColumn tableColumnId;
    public TableColumn tableColumnName;
    public TableColumn tableColumnCountry;
    public TableColumn tableColumnCity;
    public TableColumn tableColumnAddress;
    public TableColumn tableColumnGender;
    public TableColumn tableColumnBirthday;
    public TextField txtFilterName;
    public TextField txtFilterCountry;
    public Button btnBack;
    public Button btnAdd;
    public Button btnDelete;
    public Button btnClear;
    public Button btnUndo;
    public Button btnRedo;


    private ClientService clientService;
    private ObservableList<Client> clients = FXCollections.observableArrayList();



    /**
     * This method is automatically called
     * after the fxml file has been loaded.
     *
     * Initializes the table columns and sets up sorting and filtering.
     */
    @FXML
    private void initialize() throws IOException {

        // Instantiate DB config class, validator, repository and service
        ConnectToDB conn = new ConnectToDB();
        conn.connectToDB();
        clientService = conn.getClientService();

        System.out.println("Clients from DB:" + clientService.getAll());

        Platform.runLater(() -> {
            clients.addAll(clientService.getAll());
            tableViewClients.setItems(clients);

            //  Wrap the ObservableList in a FilteredList (initially display all data).
            FilteredList<Client> filteredData = new FilteredList<>(clients, p -> true);

            //  Set the filter Predicate whenever the filter changes.
            txtFilterName.textProperty().addListener((observable1, oldValue1, newValue1) -> {
                filteredData.setPredicate(client -> {
                    // If filter text is empty, display all reservations.
                    if (newValue1 == null || newValue1.isEmpty()) {
                        return true;
                    }
                    // Compare first name and last name of every person with filter text.
                    String lowerCaseFilter = newValue1.toLowerCase();
                    if (client.getName().toLowerCase().contains(lowerCaseFilter)) {
                        return true; // Filter matches name of client.
                    }
                    return false; // Does not match.
                });

                txtFilterCountry.textProperty().addListener((observable2, oldValue2, newValue2) -> {
                    filteredData.setPredicate(client -> {
                        // If filter text is empty, display all reservations.
                        if (newValue2 == null || newValue2.isEmpty()) {
                            return true;
                        }
                        String lowerCaseFilter = newValue2.toLowerCase();
                        if (client.getCountry().toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }
                        return false;
                    });
            });

            // Wrap the FilteredList in a SortedList.
            SortedList<Client> sortedData = new SortedList<>(filteredData);

            // Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(tableViewClients.comparatorProperty());

            // Add sorted (and filtered) data to the table.
            tableViewClients.setItems(sortedData);
        });
        });
    }


    public void btnAddClick(ActionEvent actionEvent) {
            try {
                Parent addReservationParent = FXMLLoader.load(getClass().getResource("/accommodationAdd.fxml"));
                Scene addReservationScene = new Scene(addReservationParent);
                Stage stage = (Stage) btnAdd.getScene().getWindow();
                stage.setTitle("Accommodation add");
                stage.setScene(addReservationScene);
                stage.show();

            } catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new window: Reservation add.", e);
            }


    }


    public void editClient(TableColumn.CellEditEvent cellEditEvent) {
        Client editedClient = (Client) cellEditEvent.getRowValue();
        try {
            String newName = (String)cellEditEvent.getNewValue();
            Client client = new Client(newName, editedClient.getGender(), editedClient.getBirthday(), editedClient.getCountry(), editedClient.getCity(), editedClient.getAddress());
            client.setId(editedClient.getId());
            clientService.update(client);
            editedClient.setName(newName);
        } catch (RuntimeException | CustomAccommodationException rex) {
            AlertBox.showValidationError(rex.getMessage());
        }
        tableViewClients.refresh();
    }


    public void btnClientDeleteClick(ActionEvent actionEvent) {
        Client selected = (Client) tableViewClients.getSelectionModel().getSelectedItem();
        if (selected != null) {
            try {
                clientService.remove(selected.getId());
                clients.clear();
                clients.addAll(clientService.getAll());
            } catch (RuntimeException rex) {
                AlertBox.showValidationError(rex.getMessage());
            }
        }
    }

    public void btnClientUndoClick(ActionEvent actionEvent) throws Exception {
        clientService.undo();
        clients.clear();
        clients.addAll(clientService.getAll());
    }

    public void btnClientRedoClick(ActionEvent actionEvent) throws Exception {
        clientService.redo();
        clients.clear();
        clients.addAll(clientService.getAll());
    }

    public void btnClientClearClick(ActionEvent actionEvent) {
        clientService.clear();
        clients.clear();
        clients.addAll(clientService.getAll());
    }


    public void btnBackClick(ActionEvent actionEvent) {
        try {
            Parent viewReportsParent = FXMLLoader.load(getClass().getResource("/sample.fxml"));
            Scene viewReportScene = new Scene(viewReportsParent);

            // Get the stage information from Main
            Node node = (Node) actionEvent.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            stage.setScene(viewReportScene);
        } catch (
                IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to go back to first page", e);
        }
    }

}






