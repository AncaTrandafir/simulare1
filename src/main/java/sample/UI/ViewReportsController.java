package sample.UI;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import sample.DBConfig.DBProperties;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ViewReportsController implements Initializable {
    public Button goBack;
    public SplitMenuButton viewReports;
    public StackedBarChart monthlyReportForCurrentYear;
    public LineChart yearlyReport;
    public LineChart evolutionForEachYear;
    public LineChart emptyChart;
    public CategoryAxis xMonthlyEvolution;
    public NumberAxis yMonthlyEvolution;
    public CategoryAxis xAllYears;
    public NumberAxis yAllYears;
    public CategoryAxis xEachYear;
    public NumberAxis yEachYear;
    public Button btnDynamicTableMonthlyBalance;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        configureOverlayChart(emptyChart, true);
    }


    public void goBack(ActionEvent actionEvent) {
        try {
            Parent viewReportsParent = FXMLLoader.load(getClass().getResource("/sample.fxml"));
            Scene viewReportScene = new Scene(viewReportsParent);

            // Get the stage information from Main
            Node node = (Node) actionEvent.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            stage.setScene(viewReportScene);
        } catch (
                IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to go back to first page", e);
        }
    }



    // We assign for each method in select a different chart which is set invisible by default. When calling the method, set it visible.
    // Thus when calling method after method informatiile nu se suprapun
    private void configureOverlayChart(final XYChart<String, Double> chart, Boolean value) {
        chart.setVisible(value);
    }

    // Report on the last 3 years
    public void viewComparisonOnMonthsClick(ActionEvent actionEvent) throws IOException {
        // Set the other charts back invisible
        configureOverlayChart(monthlyReportForCurrentYear, false);
        configureOverlayChart(yearlyReport, false);
        configureOverlayChart(emptyChart, false);

        xAllYears.setLabel("Month");
        yAllYears.setLabel("Total revenue");

        String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        // Count the different years we have in DB
        String queryCountDistictYears = "select count(distinct date_part(\'year\', checkin)) from accommodations;";
        // Get the different years and store in an arrayList at iterate later the array
        String queryDistinctYears = "select distinct date_part(\'year\', checkin) from viewaccommodations order by date_part(\'year\', checkin);;";

        ArrayList distinctYears = new ArrayList();
        DBProperties dbProperties= new DBProperties();

        try {
            Connection connection = DriverManager.getConnection(dbProperties.getURL(), dbProperties.getUser(), dbProperties.getPassword());
            PreparedStatement ps1 = connection.prepareStatement(queryCountDistictYears);
            ResultSet rs1 = ps1.executeQuery();
            PreparedStatement ps2 = connection.prepareStatement(queryDistinctYears);
            ResultSet rs2 = ps2.executeQuery();
           
            int countDistinctYears = 0;
            while (rs1.next())
                 countDistinctYears = Integer.parseInt(rs1.getString(1));

            while (rs2.next())
                distinctYears.add(Integer.parseInt(rs2.getString(1)));

            System.out.println(distinctYears);

            ObservableList<XYChart.Series<String, Number>> data = FXCollections.<XYChart.Series<String, Number>>observableArrayList();
            // loop for each year
            for (int i = 0; i < countDistinctYears; i++) {
                    XYChart.Series series = new XYChart.Series();
                    // set the name of the series as the year it represents
                    series.setName(distinctYears.get(i).toString());
                    // query for each disticnt.Years.get(i)
                    String query = "select date_part(\'month\', checkin) as mon, date_part(\'year\', checkin) as an, sum(balance) as suma from viewaccommodations where date_part(\'year\', checkin) = " + distinctYears.get(i) + " group by mon, an;";
                    PreparedStatement ps = connection.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();

                    // months - index 1 in rs, year - index 2 in rs,  sum of balances group by month - index 3
                    while (rs.next()) {
                        String month = rs.getString(1);
                     //   int year = Integer.parseInt(rs.getString(2));
                        double sum = Double.parseDouble(rs.getString(3));

//                        // concatenare month, year
//                        String date = month + ", " + year;
//                        System.out.println(date);
                        series.getData().add(new XYChart.Data<String, Number>(month, sum));
                        data.addAll(series);

                        dynamicTableViewForChart(query);
                    }

               // ObservableList<XYChart.Series<String, Number>> data = FXCollections.<XYChart.Series<String, Number>>observableArrayList();
              //  data.addAll(series);
                 //   ObservableList<XYChart.Series<String,Number>> chartData = data;
              //  evolutionForEachYear.setData(chartData);
                    evolutionForEachYear.getData().addAll(series);

                    configureOverlayChart(evolutionForEachYear,true);

                }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }



    }



        // Monthly report for current year
    public void viewMonthlyBalanceClick(ActionEvent actionEvent) throws IOException {
        configureOverlayChart(evolutionForEachYear, false);
        configureOverlayChart(yearlyReport, false);
        configureOverlayChart(emptyChart, false);

        xMonthlyEvolution.setLabel("Month");
        yMonthlyEvolution.setLabel("Total revenue");

        // Filter the data directly with query from DB, with date_part('month', date) and date_part('year', date) for the current year
        String currentYear = "select date_part(\'month\', checkin) as mon, date_part(\'year\', checkin) as an, sum(balance) as suma, sum(restaurant) as restaurant from viewaccommodations where date_part(\'year\', checkin) = date_part(\'year\', CURRENT_DATE) group by mon, an order by mon, an;";

        // xAxis - months (01, 02 ... format), yAxis - sum

       // StackedBarChart de 2 serii: balance si restaurant
        monthlyReportForCurrentYear.setTitle("Present year");

        XYChart.Series<String, Number> balanceSeries = new XYChart.Series<String, Number>();
        XYChart.Series<String, Number> restaurantSeries = new XYChart.Series<String, Number>();

     //   xMonthlyEvolution.setCategories(FXCollections.observableArrayList(Arrays.asList("Ian", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")));

        DBProperties dbProperties= new DBProperties();
        try {
            Connection connection = DriverManager.getConnection(dbProperties.getURL(), dbProperties.getUser(), dbProperties.getPassword());
            PreparedStatement ps = connection.prepareStatement(currentYear);
            ResultSet rs = ps.executeQuery();

            // months - index 1 in rs, year - index 2 in rs,  sum of balances group by month - index 3
            while (rs.next()) {
                int month = Integer.parseInt(rs.getString(1));
                int year = Integer.parseInt(rs.getString(2));
                double balance = Double.parseDouble(rs.getString(3));
                double restaurant = Double.parseDouble(rs.getString(4));

                // concatenare month, year
                String date = month + ", " + year;
                balanceSeries.getData().add(new XYChart.Data<String, Number>(date, balance));
                System.out.println(balanceSeries.getData());
                restaurantSeries.getData().add(new XYChart.Data<String, Number>(date, restaurant));
                System.out.println(restaurantSeries.getData());
            }
            monthlyReportForCurrentYear.getData().addAll(balanceSeries, restaurantSeries);

            // Set the chart visible
            configureOverlayChart(monthlyReportForCurrentYear, true);


        }  catch (
                SQLException e) {
            e.printStackTrace();
        }
        dynamicTableViewForChart(currentYear);

    }




        // Report for every single year
    public void viewYearlyBalanceClick(ActionEvent actionEvent) throws IOException {
        // chart
        configureOverlayChart(monthlyReportForCurrentYear, false);
        configureOverlayChart(monthlyReportForCurrentYear, false);
        configureOverlayChart(emptyChart, false);

        xEachYear.setLabel("Year");
        yEachYear.setLabel("Total revenue");

        DBProperties dbProperties= new DBProperties();

        String query = "select date_part(\'year\', checkin) as an, sum(balance) as suma from viewaccommodations group by an order by an;";
        XYChart.Series<String,Double> series = new XYChart.Series<>();
        series.setName("Evolutie anuala");
        try {
            Connection connection = DriverManager.getConnection(dbProperties.getURL(), dbProperties.getUser(), dbProperties.getPassword());
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String year = rs.getString(1);
                double sum = Double.parseDouble(rs.getString(2));

                series.getData().add(new XYChart.Data<String, Double>(year, sum));
            }

            yearlyReport.getData().add(series);
            // Set the chart visible
            configureOverlayChart(yearlyReport, true);


        }  catch (
                SQLException e) {
            e.printStackTrace();
        }

        // Call dynamicTable
        dynamicTableViewForChart(query);
    }




// dynamic table which sets a new window and reflects data from the chart in a table view, parameter query for the db
    public void dynamicTableViewForChart(String sql) {

        TableView tableview = new TableView();
        ObservableList<ObservableList> data = FXCollections.observableArrayList();

       // String sql = "select date_part(\'month\', checkin) as month, date_part(\'year\', checkin) as year, sum(balance) as accommodation, sum(restaurant) from viewaccommodations where date_part(\'year\', checkin) = date_part(\'year\', CURRENT_DATE) group by mon, an order by mon, an;";

        CustomResultSetTableView viewMonthlyBalance = new CustomResultSetTableView(tableview, data);
        viewMonthlyBalance.buildData(sql);

        StackPane secondaryLayout = new StackPane();
        secondaryLayout.getChildren().add(viewMonthlyBalance);

        try {
            Stage newStage = new Stage();
            Scene secondScene = new Scene(tableview, 250,400);
            newStage.setTitle("Evolution of revenues");
            newStage.setScene(secondScene);
            newStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
