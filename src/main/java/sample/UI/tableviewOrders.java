package sample.UI;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.converter.DoubleStringConverter;
import sample.DBConfig.ConnectToDB;
import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.Restaurant.Order;

import sample.Service.OrderService;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class tableviewOrders {
    @FXML
    public TableView tableViewOrders;
    public TableColumn tableColumnId;
    public Button btnBack;
    public Button btnAdd;
    public Button btnDelete;
    public Button btnClear;
    public Button btnUndo;
    public Button btnRedo;
    public TableColumn tableColumnIdAccommodation;
    public TableColumn tableColumnOrderDate;
    public TableColumn tableColumnOrderValue;
    public TextField txtFilterById;

    private OrderService orderService;
    private ObservableList<Order> orders = FXCollections.observableArrayList();


    /**
     * This method is automatically called
     * after the fxml file has been loaded.
     *
     * Initializes the table columns and sets up sorting and filtering.
     */
    @FXML
    private void initialize() throws IOException {

        // by default cellFactory is type String, have to force it to double to edit ordervalue column
        tableColumnOrderValue.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));

        // Instantiate DB config class, validator, repository and service
        ConnectToDB conn = new ConnectToDB();
        conn.connectToDB();
        orderService = conn.getOrderService();

        Platform.runLater(() -> {
            orders.addAll(orderService.getAll());
            System.out.println("Orders from DB:" + orders);
            tableViewOrders.setItems(orders);

            //  Wrap the ObservableList in a FilteredList (initially display all data).
            FilteredList<Order> filteredData = new FilteredList<>(orders, p -> true);

            //  Set the filter Predicate whenever the filter changes.
            txtFilterById.textProperty().addListener((observable1, oldValue1, newValue) -> {
                filteredData.setPredicate(order -> {
                    // If filter text is empty, display all reservations.
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }
                    int newValueInt = Integer.parseInt(newValue);
                    if (order.getIdAccommodation() == newValueInt) {
                        return true;
                    }

                    return false; // Does not match.
                });

                // Wrap the FilteredList in a SortedList.
                SortedList<Order> sortedData = new SortedList<>(filteredData);

                // Bind the SortedList comparator to the TableView comparator.
                sortedData.comparatorProperty().bind(tableViewOrders.comparatorProperty());

                // Add sorted (and filtered) data to the table.
                tableViewOrders.setItems(sortedData);
            });
        });
    }



    public void btnAddClick(ActionEvent actionEvent) {
        try {
            Parent addReservationParent = FXMLLoader.load(getClass().getResource("/accommodationAdd.fxml"));
            Scene addReservationScene = new Scene(addReservationParent);
            Stage stage = (Stage) btnAdd.getScene().getWindow();
            stage.setTitle("Accommodation add");
            stage.setScene(addReservationScene);
            stage.show();

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new window: Reservation add.", e);
        }


    }


    public void editValue(TableColumn.CellEditEvent cellEditEvent) {
        Order editedOrder = (Order) cellEditEvent.getRowValue();
        try {
            // cellEditEvent always String in tableview but we converted it in initialize in double type
            Double newValue = (Double) cellEditEvent.getNewValue();
            Order order = new Order(editedOrder.getIdAccommodation(), editedOrder.getOrderDate(), newValue);
            order.setId(editedOrder.getId());
            orderService.update(order);
            editedOrder.setOrderValue(newValue);
        } catch (RuntimeException | CustomAccommodationException rex) {
            AlertBox.showValidationError(rex.getMessage());
        }
        tableViewOrders.refresh();
    }


    public void btnOrderDeleteClick(ActionEvent actionEvent) {
        Order selected = (Order) tableViewOrders.getSelectionModel().getSelectedItem();
        if (selected != null) {
            try {
                orderService.remove(selected.getId());
                orders.clear();
                orders.addAll(orderService.getAll());
            } catch (RuntimeException rex) {
                AlertBox.showValidationError(rex.getMessage());
            }
        }
    }

    public void btnOrderUndoClick(ActionEvent actionEvent) throws Exception {
        orderService.undo();
        orders.clear();
        orders.addAll(orderService.getAll());
    }

    public void btnOrderRedoClick(ActionEvent actionEvent) throws Exception {
        orderService.redo();
        orderService.clear();
        orders.addAll(orderService.getAll());
    }

    public void btnOrderClearClick(ActionEvent actionEvent) {
        orderService.clear();
        orders.clear();
        orders.addAll(orderService.getAll());
    }


    public void btnBackClick(ActionEvent actionEvent) {
        try {
            Parent viewReportsParent = FXMLLoader.load(getClass().getResource("/sample.fxml"));
            Scene viewReportScene = new Scene(viewReportsParent);

            // Get the stage information from Main
            Node node = (Node) actionEvent.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            stage.setScene(viewReportScene);
        } catch (
                IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to go back to first page", e);
        }
    }

}






