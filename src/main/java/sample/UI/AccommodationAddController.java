package sample.UI;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.TextFields;

import sample.DBConfig.ConnectToDB;
import sample.Domain.Accommodation.Accommodation;
import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.Client.Client;
import sample.Domain.Company.Company;
import sample.Domain.Restaurant.Order;
import sample.Service.AccommodationService;
import sample.Service.ClientService;
import sample.Service.CompanyService;
import sample.Service.OrderService;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

public class AccommodationAddController implements Initializable {
    public TextField txtClient;
    public TextField txtRoomRate;
    public Button btnAdd;
    public Button btnCancel;
    public DatePicker calendarCI;
    public DatePicker calendarCO;
    public TextField txtCompany;
    public TextField txtAgency;
    public ChoiceBox<String> choiceBoxPriceType;
    public ChoiceBox<String> choiceBoxRoomType;
    public TextField txtNameClient;
    public TextField txtCityClient;
    public TextField txtAddressClient;
    public ToggleGroup genderToggle;
    public Button btnAddClient;
    public Button btnCancelClient;
    public TextField txtNameCompany;
    public TextField txtCIFCompany;
    public TextField txtRegComCompany;
    public TextField txtCityCompany;
    public TextField txtAddressCompany;
    public Button btnCancelCompany;
    public Button btnAddCompany;
    public DatePicker calBirthdayClient;
    public ComboBox comboBoxCountryCompany;
    public ComboBox comboBoxCountryClient;
    public TextField txtValOrder;
    public DatePicker calDateOrder;
    public Button btnCancelOrder;
    public Button btnAddOrder;
    public TextField txtIdAccommodationOrder;

    // Accpmmodation add controller
    private AccommodationService accommodationService;
    private ClientService clientService;
    private CompanyService companyService;
    private OrderService orderService;

    // Constructor
    public AccommodationAddController() {
    }

    private String priceType;
    private String roomType;
    private String countryClient;
    private String countryCompany;
    private String gender;

    final ObservableList listPriceType = FXCollections.observableArrayList("MD", "FMD", "20%", "50%", "PTC");
    final ObservableList listRoomType = FXCollections.observableArrayList("LG", "TW", "AP", "SG");
    final ObservableList listCountries = FXCollections.observableArrayList("Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe");

    private ObservableList<Accommodation> accommodations = FXCollections.observableArrayList();
    private ObservableList<Client> clients = FXCollections.observableArrayList();
    private ObservableList<Company> companies = FXCollections.observableArrayList();
    private ObservableList<Order> orders = FXCollections.observableArrayList();

    // autocomplete textfield fro client and company with suggestions from DB
    private ArrayList<String> clientNameSuggestions = new ArrayList<>();
    private ArrayList<String> companyNameSuggestions = new ArrayList<>();

    int idClient = 0;
    int idCompany = 0;


    // choiceBox change listener
    public void loadDataPriceType() {
        choiceBoxPriceType.setItems(listPriceType);
        // add a listener
        choiceBoxPriceType.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                priceType = newValue;
            }
        });
    }

    // choiceBox change listener
    public void loadDataRoomType() {
        choiceBoxRoomType.setItems(listRoomType);
        // add a listener
        choiceBoxRoomType.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                // de tip: ObjectProperty [bean: ChoiceBox[id=choiceBoxPriceType, styleClass=choice-box], name: value, value: 20%]
                // System.out.println(observable);
                // valoarea anterioara selectata
                // System.out.println(oldValue);
                // valoarea curenta
                roomType = newValue;
            }
        });
    }

    // ComboBox Country change listener
    public void loadCountryComboBox() {
        comboBoxCountryClient.setItems(listCountries);
        comboBoxCountryClient.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                countryClient = newValue;
            }
        });

        comboBoxCountryCompany.setItems(listCountries);
        comboBoxCountryCompany.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                countryCompany = newValue;
            }
        });
    }

    public void radioToggleGender() {
        // Add change listener for radio toggle group
        genderToggle.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) {

                RadioButton chk = (RadioButton)t1.getToggleGroup().getSelectedToggle(); // Cast object to radio button
                gender = chk.getText();

            }
        });
    }



    public void autoCompleteClientName() throws IOException {
        ConnectToDB conn = new ConnectToDB();
        conn.connectToDB();
        clientService = conn.getClientService();
        clients.addAll(clientService.getAll());
        for (Client c : clients)
            clientNameSuggestions.add(c.getName());
        TextFields.bindAutoCompletion(txtClient, clientNameSuggestions);
    }

    public void autoCompleteCompanyName() throws IOException {
        ConnectToDB conn = new ConnectToDB();
        conn.connectToDB();
        companyService = conn.getCompanyService();
        companies.addAll(companyService.getAll());
        for (Company c : companies)
            companyNameSuggestions.add(c.getName());
        TextFields.bindAutoCompletion(txtCompany, companyNameSuggestions);
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try{
            autoCompleteCompanyName();
            autoCompleteClientName();
            loadDataPriceType();
            loadDataRoomType();
            loadCountryComboBox();
            radioToggleGender();

//        try {
//            validateClientInDB();
//        }catch (Exception ex) {
//            String message = "No entry found for this client in the database. Please register a new profile.";
//            AlertBox.showValidationError(message);
//            throw new RuntimeException(ex.getMessage()); }
//
//        try {
//            validateCompanyInDB();
//        }catch (Exception ex) {
//            String message = "No entry found for this company in the database. Please register a new profile.";
//            AlertBox.showValidationError(message);
//            throw new RuntimeException(ex.getMessage()); }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setService(AccommodationService accommodationService, ClientService clientService, CompanyService companyService) {
        this.accommodationService = accommodationService;
        this.clientService = clientService;
        this.companyService = companyService;
    }

    public void btnCancelClick(ActionEvent actionEvent) {
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }

    public void btnCancelClientClick(ActionEvent actionEvent) {
        Stage stage = (Stage) btnCancelClient.getScene().getWindow();
        stage.close();
    }

    public void btnCancelCompanyClick(ActionEvent actionEvent) {
        Stage stage = (Stage) btnCancelCompany.getScene().getWindow();
        stage.close();
    }

    public void btnCancelOrderClick(ActionEvent actionEvent) {
        Stage stage = (Stage) btnCancelOrder.getScene().getWindow();
        stage.close();
    }

    // for  client add tab
    public int validateClientInAccommodationTab() throws IOException {
            ConnectToDB conn = new ConnectToDB();
            conn.connectToDB();
            clientService = conn.getClientService();
            clients.addAll(clientService.getAll());
            // Validation that client is already registred in DB.
            for (Client c : clients) {
                if (c.getName().toLowerCase().equals(txtClient.getText().toLowerCase())) {
                   idClient = c.getId();
                }
            }
            return idClient;
    }

    // for oder add tab
//    public int validateClientInOrderTab() throws IOException {
//        ConnectToDB conn = new ConnectToDB();
//        conn.connectToDB();
//        clientService = conn.getClientService();
//        clients.addAll(clientService.getAll());
//        // Validation that client is already registred in DB.
//        for (Client c : clients) {
//            if (c.getName().toLowerCase().equals(txtClientOrder.getText().toLowerCase())) {
//                idClient = c.getId();
//            }
//        }
//        return idClient;
//    }

    public int validateCompanyInCompanyTab() throws IOException {
         // Validation that company is already registred in DB.
             ConnectToDB conn = new ConnectToDB();
             conn.connectToDB();
             companyService = conn.getCompanyService();
             companies.addAll(companyService.getAll());
             // Retrieve company by name
            for (Company c : companies) {
                if (c.getName().toLowerCase().equals(txtCompany.getText().toLowerCase())) {
                    idCompany = c.getId();
                }
            }
        return idCompany;
    }

    public void validateOrderDateInOrderTab() throws IOException {
        // Validation that order date is between checkin and checkout of accommodation.get = order.getIdAccommodation.
        // we iterate through accommodations
        ConnectToDB conn = new ConnectToDB();
        conn.connectToDB();
        accommodationService = conn.getAccommodationService();
        accommodations.addAll(accommodationService.getAll());
        // Retrieve order by idaccommodation
        for (Accommodation a : accommodations) {
            if ( a.getId() == Integer.parseInt(txtIdAccommodationOrder.getText()) && a.getCheckIn().isBefore(calDateOrder.getValue()) && a.getCheckOut().isAfter(calDateOrder.getValue())) {
                System.out.println("data order validata");
            }
        }
    }


    public void btnAddAccommodationClick(ActionEvent actionEvent) throws CustomAccommodationException {
        // id of accommodation and balance value don't matter because id is auto-incremented, so it is overwritten.
        try {
            int id = 0;
            String clientString = txtClient.getText();
            String companyString = txtCompany.getText();
            LocalDate CI = calendarCI.getValue();
            LocalDate CO = calendarCO.getValue();
            double roomRate = Double.parseDouble(txtRoomRate.getText());

            Accommodation accommodation = new Accommodation(clientString, companyString, CI, CO, roomRate, priceType, roomType);
            accommodation.setId(id); // obiectul accommodation este procesat mai departe in repository

                        try {
                            idClient = validateClientInAccommodationTab();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println(idClient);
                        try {
                            idCompany = validateCompanyInCompanyTab();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println(idCompany);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

                        String sqlInsert = "insert into accommodations1 (idclient, idcompany, checkIn, checkOut, roomRate, priceType, roomType) values (?, ?, ?, ?, ?, ?, ?);";

                        String url = "jdbc:postgresql://localhost:5432/HotelMng";
                        String user = "postgres";
                        String password = "hennazara";

                        try {
                            Connection connection = DriverManager.getConnection(url, user, password);
                            PreparedStatement ps = connection.prepareStatement(sqlInsert); {

                                    // anexam idClient si idCompany la tabelul accommodations1, nu la view. view e procesat in JDBCRepo
                                    ps.setInt(1, idClient);
                                    ps.setInt(2, idCompany);
                                    ps.setDate(3, Date.valueOf(simpleDateFormat.format(Date.valueOf(accommodation.getCheckIn()))));
                                    ps.setDate(4, Date.valueOf(simpleDateFormat.format(Date.valueOf(accommodation.getCheckOut()))));
                                    ps.setDouble(5, accommodation.getRoomRate());
                                    ps.setString(6, accommodation.getPriceType());
                                    ps.setString(7, accommodation.getRoomType());
                                }

                                ps.executeUpdate();
                            accommodationService.add(accommodation);
                            System.out.println(accommodation);
                            btnCancelClick(actionEvent);

                            } catch (SQLException e ) {
                                String message = "No entry found for this client / company in the database. Please register a new profile for client / company.";
                                AlertBox.showValidationError(message);
                                e.printStackTrace();
                        }

        } catch (RuntimeException rex) {
            AlertBox.showValidationError(rex.getMessage());
            throw new CustomAccommodationException(rex.getMessage());
        }

    }


    // Clieent add controller
    public void btnAddClientClick(ActionEvent actionEvent) {
        try {
            ConnectToDB conn = new ConnectToDB();
            conn.connectToDB();
            clientService = conn.getClientService();
        int id = 0;
        // Pt clien, company, agency facem cautare dupa nume
        String name = txtNameClient.getText();
        LocalDate birthday = calBirthdayClient.getValue();
        // gender si country sunt iitializate in initialize;
            String city = txtCityClient.getText();
            String address = txtAddressClient.getText();

        Client client = new Client(name, gender, birthday, countryClient, city, address);
        client.setId(id);
        clientService.add(client);
        btnCancelClientClick(actionEvent);

    } catch (IOException rex) {
        AlertBox.showValidationError(rex.getMessage());
        System.out.println("Error:> "+rex);
    }

}

    public void btnAddCompanyClick(ActionEvent actionEvent) throws CustomAccommodationException {
          try{
              ConnectToDB conn = new ConnectToDB();
              conn.connectToDB();
              companyService = conn.getCompanyService();
            int id = 0;
            // Pt clien, company, agency facem cautare dupa nume
            String name = txtNameCompany.getText();
            String CIF = txtCIFCompany.getText();
            String regCom = txtRegComCompany.getText();
            // country sunt iitializate in initialize;
            String city = txtCityCompany.getText();
            String address = txtAddressCompany.getText();

            Company company = new Company(name, CIF, regCom, countryCompany, city, address);
            company.setId(id);
            companyService.add(company);
            btnCancelCompanyClick(actionEvent);

        } catch (RuntimeException | IOException rex) {
            System.out.println("Error:> "+rex);
            AlertBox.showValidationError(rex.getMessage());

        }

    }


    public void btnAddOrderClick(ActionEvent actionEvent) {
        try {
            ConnectToDB conn = new ConnectToDB();
            conn.connectToDB();
            orderService = conn.getOrderService();

                int id = 0;
                int idAccommodation = Integer.parseInt(txtIdAccommodationOrder.getText());
                LocalDate dateOrder = calDateOrder.getValue();
                double valueOrder = Double.parseDouble(txtValOrder.getText());

                try {
                    validateOrderDateInOrderTab();
                } catch (RuntimeException | IOException rex) {
                System.out.println("Error:> "+rex);
                AlertBox.showValidationError(rex.getMessage());

            }



                Order order = new Order(idAccommodation, dateOrder, valueOrder);
                order.setId(id);
                orderService.add(order);

            btnCancelCompanyClick(actionEvent);

        } catch (RuntimeException | IOException rex) {
            System.out.println("Error:> "+rex);
            AlertBox.showValidationError(rex.getMessage());
        }

    }


}
