package sample.UI;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sample.DBConfig.ConnectToDB;

import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.Client.Client;
import sample.Domain.Company.Company;
import sample.Service.CompanyService;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class tableviewCompanies {
    @FXML
    public TableView tableViewCompanies;
    public TableColumn tableColumnId;
    public TableColumn tableColumnName;
    public TableColumn tableColumnCountry;
    public TableColumn tableColumnCity;
    public TableColumn tableColumnAddress;
    public TextField txtFilterName;
    public Button btnBack;
    public Button btnAdd;
    public Button btnDelete;
    public Button btnClear;
    public Button btnUndo;
    public Button btnRedo;
    public TableColumn tableColumnCif;
    public TableColumn tableColumnRegCom;
    public TextField txtFilterCity;

    private CompanyService companyService;
    private ObservableList<Company> companies = FXCollections.observableArrayList();
    

    /**
     * This method is automatically called
     * after the fxml file has been loaded.
     *
     * Initializes the table columns and sets up sorting and filtering.
     */
    @FXML
    private void initialize() throws IOException {

        // Instantiate DB config class, validator, repository and service
        ConnectToDB conn = new ConnectToDB();
        conn.connectToDB();
        companyService = conn.getCompanyService();

        System.out.println("Clients from DB:" + companyService.getAll());

        Platform.runLater(() -> {
            companies.addAll(companyService.getAll());
            tableViewCompanies.setItems(companies);

            //  Wrap the ObservableList in a FilteredList (initially display all data).
            FilteredList<Company> filteredData = new FilteredList<>(companies, p -> true);

            //  Set the filter Predicate whenever the filter changes.
            txtFilterName.textProperty().addListener((observable1, oldValue1, newValue1) -> {
                filteredData.setPredicate(company -> {
                    // If filter text is empty, display all reservations.
                    if (newValue1 == null || newValue1.isEmpty()) {
                        return true;
                    }
                    // Compare first name and last name of every person with filter text.
                    String lowerCaseFilter = newValue1.toLowerCase();
                    if (company.getName().toLowerCase().contains(lowerCaseFilter)) {
                        return true; // Filter matches name of company.
                    }
                    return false; // Does not match.
                });

                txtFilterCity.textProperty().addListener((observable2, oldValue2, newValue2) -> {
                    filteredData.setPredicate(company -> {
                        // If filter text is empty, display all companies.
                        if (newValue2 == null || newValue2.isEmpty()) {
                            return true;
                        }
                        String lowerCaseFilter = newValue2.toLowerCase();
                        if (company.getCountry().toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }
                        return false;
                    });
                });

                // Wrap the FilteredList in a SortedList.
                SortedList<Company> sortedData = new SortedList<>(filteredData);

                // Bind the SortedList comparator to the TableView comparator.
                sortedData.comparatorProperty().bind(tableViewCompanies.comparatorProperty());

                // Add sorted (and filtered) data to the table.
                tableViewCompanies.setItems(sortedData);
            });
        });
    }


    public void btnAddClick(ActionEvent actionEvent) {
        try {
            Parent addReservationParent = FXMLLoader.load(getClass().getResource("/accommodationAdd.fxml"));
            Scene addReservationScene = new Scene(addReservationParent);
            Stage stage = (Stage) btnAdd.getScene().getWindow();
            stage.setTitle("Accommodation add");
            stage.setScene(addReservationScene);
            stage.show();

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new window: Reservation add.", e);
        }


    }


    public void editName(TableColumn.CellEditEvent cellEditEvent) {
        Company editedCompany = (Company) cellEditEvent.getRowValue();
        try {
            String newName = (String)cellEditEvent.getNewValue();
            Company company = new Company(newName, editedCompany.getCif(), editedCompany.getRegCom(), editedCompany.getCountry(), editedCompany.getCity(), editedCompany.getAddress());
            company.setId(editedCompany.getId());
            companyService.update(company);
            editedCompany.setName(newName);
        } catch (RuntimeException | CustomAccommodationException rex) {
            AlertBox.showValidationError(rex.getMessage());
        }
        tableViewCompanies.refresh();
    }


    public void btnCompanyDeleteClick(ActionEvent actionEvent) {
        Company selected = (Company) tableViewCompanies.getSelectionModel().getSelectedItem();
        if (selected != null) {
            try {
                companyService.remove(selected.getId());
                companies.clear();
                companies.addAll(companyService.getAll());
            } catch (RuntimeException rex) {
                AlertBox.showValidationError(rex.getMessage());
            }
        }
    }

    public void btnCompanyUndoClick(ActionEvent actionEvent) throws Exception {
        companyService.undo();
        companies.clear();
        companies.addAll(companyService.getAll());
    }

    public void btnCompanyRedoClick(ActionEvent actionEvent) throws Exception {
        companyService.redo();
        companies.clear();
        companies.addAll(companyService.getAll());
    }

    public void btnCompanyClearClick(ActionEvent actionEvent) {
        companyService.clear();
        companies.clear();
        companies.addAll(companyService.getAll());
    }


    public void btnBackClick(ActionEvent actionEvent) {
        try {
            Parent viewReportsParent = FXMLLoader.load(getClass().getResource("/sample.fxml"));
            Scene viewReportScene = new Scene(viewReportsParent);

            // Get the stage information from Main
            Node node = (Node) actionEvent.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            stage.setScene(viewReportScene);
        } catch (
                IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to go back to first page", e);
        }
    }

}






