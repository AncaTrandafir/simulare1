package sample.UI;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomResultSetTableView extends TableView {

    private ObservableList<ObservableList> data;
    private TableView tableView;

    public CustomResultSetTableView(TableView tableView, ObservableList<ObservableList> data){
        this.tableView = tableView;
        this.data = data;
    }

    public String url = "jdbc:postgresql://localhost:5432/HotelMng";
    public String user = "postgres";
    public String password = "hennazara";



    public void buildData(String sql) {
        data = FXCollections.observableArrayList();
    try {
        Connection connection = DriverManager.getConnection(url, user, password);
        ResultSet rs = connection.createStatement().executeQuery(sql);

            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                //We are using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<ObservableList, String> param) {
                        return new SimpleStringProperty(param.getValue().get(j).toString());
                    }
                });

                tableView.getColumns().addAll(col);
                System.out.println("Column [" + i + "] ");
            }

            while (rs.next()) {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rs.getString(i));
                }
                System.out.println("Row [1] added " + row);
                data.add(row);
            }

                tableView.setItems(data);

        } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error on Building Data");
            }
    }



}
