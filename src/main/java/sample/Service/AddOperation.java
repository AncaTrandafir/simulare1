package sample.Service;

import sample.Domain.Entity;
import sample.Repository.IRepository;

public class AddOperation<ID, T extends Entity<ID>> extends UndoRedoOperation<ID, T> {
    private T addedEntity;

    public AddOperation(IRepository<ID, T> repository, T addedEntity) {
        super(repository);
        this.addedEntity = addedEntity;
    }

    @Override
    public void doUndo(){
        repository.remove(addedEntity.getId());
    }

    @Override
    public void doRedo() throws Exception {
        repository.update(addedEntity);
    }
}
