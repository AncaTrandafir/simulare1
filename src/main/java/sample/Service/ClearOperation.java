package sample.Service;

import sample.Domain.Entity;
import sample.Repository.IRepository;
import java.util.List;
import java.util.zip.DataFormatException;

public class ClearOperation<ID, T extends Entity<ID>> extends UndoRedoOperation<ID, T> {

    private List<T> clearedEntities;

    public ClearOperation(IRepository<ID, T> repository, List<T> clearedEntities) {
        super(repository);
        this.clearedEntities = clearedEntities;
    }

    @Override
    public void doUndo() throws Exception, DataFormatException {
        for (T entity : clearedEntities) {
            repository.save(entity);
        }
    }

    @Override
    public void doRedo(){
        for (T entity : clearedEntities) {
            repository.remove(entity.getId());
        }
    }
}
