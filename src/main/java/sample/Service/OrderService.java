package sample.Service;

import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.Client.Client;
import sample.Domain.Restaurant.Order;
import sample.Repository.JDBCClientRepository;
import sample.Repository.JDBCOrderRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class OrderService {

    private JDBCOrderRepository repository;
    private Stack<UndoRedoOperation<Integer, Order>> undoableOperations = new Stack<>();
    private Stack<UndoRedoOperation<Integer, Order>> redoableOperations = new Stack<>();

    /**
     * Constructs a service.
     *
     * @param repository the backing repository.
     */
    public OrderService(JDBCOrderRepository repository) {
        this.repository = repository;
    }


    /**
     * Adds a reservation with the given fields or updates an existing reservation.
     *
     * @param order - to be added.
         */
    public void add(Order order) {
        try {
            repository.save(order);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Gets a list of all reservations.
     *
     * @return a list of all invoices.
     */
    public Set<Order> getAll() {
        Iterable<Order> orders = repository.getAll();
        return StreamSupport.stream(orders.spliterator(), false).collect(Collectors.toSet());
    }

    /**
     * Gets an id whose object to remove.
     */
    public void remove(int id){
        repository.remove(id);
    }


    public Optional<Order> findById(int id) {
        return repository.findById(id);
    }
    
    public void update(Order order) throws CustomAccommodationException {
        repository.update(order);
    }


    public void undo() throws Exception {
        if (!undoableOperations.empty()) {
            UndoRedoOperation<Integer, Order> lastOperation = undoableOperations.pop();
            lastOperation.doUndo();
            redoableOperations.add(lastOperation);

        }
    }

    public void redo() throws Exception {
        if (!redoableOperations.empty()) {
            UndoRedoOperation<Integer, Order> lastOperation = redoableOperations.pop();
            lastOperation.doRedo();
            undoableOperations.add(lastOperation);
        }
    }

    public void clear() {
        List<Order> all = repository.getAll();
        for (Order o : all) {
            repository.remove(o.getId());
        }
        undoableOperations.push(new ClearOperation<>(repository, all));
        redoableOperations.clear();
    }

    public Set<Order> getOrdersBetweenDates(LocalDate startDate, LocalDate endDate) throws CustomAccommodationException {
        Iterable<Order> orders = repository.getordersBetweenDates(startDate, endDate);
        return StreamSupport.stream(orders.spliterator(), false).collect(Collectors.toSet());
    }

}



