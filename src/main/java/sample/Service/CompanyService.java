package sample.Service;

import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.Company.Company;
import sample.Repository.JDBCCompanyRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CompanyService {

    private JDBCCompanyRepository repository;
    private Stack<UndoRedoOperation<Integer, Company>> undoableOperations = new Stack<>();
    private Stack<UndoRedoOperation<Integer, Company>> redoableOperations = new Stack<>();

    /**
     * Constructs a service.
     *
     * @param repository the backing repository.
     */
    public CompanyService(JDBCCompanyRepository repository) {
        this.repository = repository;
    }


    /**
     * Adds a reservation with the given fields or updates an existing reservation.
     *
     * @param company - to be added.
         */
    public void add(Company company) {
        try {
            repository.save(company);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Gets a list of all reservations.
     *
     * @return a list of all invoices.
     */
    public Set<Company> getAll() {
        Iterable<Company> companies = repository.getAll();
        return StreamSupport.stream(companies.spliterator(), false).collect(Collectors.toSet());
    }

    /**
     * Gets an id whose object to remove.
     */
    public void remove(int id){
        repository.remove(id);
    }


    public Optional<Company> findById(int id) throws Exception{
        return repository.findById(id);
    }

    public void update(Company company) throws CustomAccommodationException {
        repository.update(company);
    }


    public void undo() throws Exception {
        if (!undoableOperations.empty()) {
            UndoRedoOperation<Integer, Company> lastOperation = undoableOperations.pop();
            lastOperation.doUndo();
            redoableOperations.add(lastOperation);

        }
    }

    public void redo() throws Exception {
        if (!redoableOperations.empty()) {
            UndoRedoOperation<Integer, Company> lastOperation = redoableOperations.pop();
            lastOperation.doRedo();
            undoableOperations.add(lastOperation);
        }
    }

    public void clear() {
        List<Company> all = repository.getAll();
        for (Company c : all) {
            repository.remove(c.getId());
        }
        undoableOperations.push(new ClearOperation<>(repository, all));
        redoableOperations.clear();
    }

}



