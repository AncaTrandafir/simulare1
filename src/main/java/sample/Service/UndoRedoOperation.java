package sample.Service;

import sample.Domain.Entity;
import sample.Repository.IRepository;

public abstract class UndoRedoOperation <ID, T extends Entity<ID>> {
    protected IRepository<ID, T> repository;

    public UndoRedoOperation(IRepository<ID, T> repository) {
        this.repository = repository;
    }

    public abstract void doUndo() throws Exception;
    public abstract void doRedo() throws Exception;
}
