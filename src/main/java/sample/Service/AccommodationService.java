package sample.Service;

import sample.Domain.Accommodation.Accommodation;
import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Repository.JDBCAccommodationRepository;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class AccommodationService {

    private JDBCAccommodationRepository repository;
    private Stack<UndoRedoOperation<Integer, Accommodation>> undoableOperations = new Stack<>();
    private Stack<UndoRedoOperation<Integer, Accommodation>> redoableOperations = new Stack<>();

    /**
     * Constructs a service.
     *
     * @param repository the backing repository.
     */
    public AccommodationService(JDBCAccommodationRepository repository) {
        this.repository = repository;
    }


    /**
     * Adds a reservation with the given fields or updates an existing reservation.
     *
     * @param accommodation - to be added.
         */
    public void add(Accommodation accommodation) {
        try {
            repository.save(accommodation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Gets a list of all reservations.
     *
     * @return a list of all invoices.
     */
    public Set<Accommodation> getAll() {
        Iterable<Accommodation> reservations = repository.getAll();
        return StreamSupport.stream(reservations.spliterator(), false).collect(Collectors.toSet());
    }

    /**
     * Gets an id whose object to remove.
     */
    public void remove(int id){
        repository.remove(id);
    }


    public Optional<Accommodation> findById(int id) throws Exception{
        return repository.findById(id);
    }
    
    public void update(Accommodation accommodation) throws CustomAccommodationException {
        repository.update(accommodation);
    }


    public void undo() throws Exception {
        if (!undoableOperations.empty()) {
            UndoRedoOperation<Integer, Accommodation> lastOperation = undoableOperations.pop();
            lastOperation.doUndo();
            redoableOperations.add(lastOperation);

        }
    }

    public void redo() throws Exception {
        if (!redoableOperations.empty()) {
            UndoRedoOperation<Integer, Accommodation> lastOperation = redoableOperations.pop();
            lastOperation.doRedo();
            undoableOperations.add(lastOperation);
        }
    }

    public void clear() {
        List<Accommodation> all = repository.getAll();
        for (Accommodation r : all) {
            repository.remove(r.getId());
        }
        undoableOperations.push(new ClearOperation<>(repository, all));
        redoableOperations.clear();
    }

    public Set<Accommodation> getReservationsBetweenDates(LocalDate startDate, LocalDate endDate) throws CustomAccommodationException {
        Iterable<Accommodation> reservations = repository.getReservationsBetweenDates(startDate, endDate);
        return StreamSupport.stream(reservations.spliterator(), false).collect(Collectors.toSet());
    }

    public int getCountReservations(LocalDate startDate, LocalDate endDate) throws CustomAccommodationException {
        int count = 0;
        List<Accommodation> all = repository.getReservationsBetweenDates(startDate, endDate);
        for (Accommodation r : all) {
            count++;
        }
        return count;
    }

    public double getMaxBalance(LocalDate startDate, LocalDate endDate) throws CustomAccommodationException {
        double max = 0;
        List<Accommodation> all = repository.getReservationsBetweenDates(startDate, endDate);
        for (Accommodation r : all) {
            double balance = r.getRoomRate() * ChronoUnit.DAYS.between(r.getCheckIn(), r.getCheckOut());
            if (balance > max)
                max = balance;
        }
        return max;
    }

    public long getNightsNumber(LocalDate startDate, LocalDate endDate) throws CustomAccommodationException {
        int totalDays = 0;
        List<Accommodation> all = repository.getReservationsBetweenDates(startDate, endDate);
        for (Accommodation r : all) {
            long days = ChronoUnit.DAYS.between(r.getCheckIn(), r.getCheckOut());
            totalDays += days;
        }
        return totalDays;
    }


    public Double getTotalrevenues(LocalDate startDate, LocalDate endDate) throws CustomAccommodationException {
        double sum = 0;
        List<Accommodation> all = repository.getReservationsBetweenDates(startDate, endDate);
        for (Accommodation r : all) {
            double balance = r.getRoomRate() * ChronoUnit.DAYS.between(r.getCheckIn(), r.getCheckOut());
            sum += balance;
        }
        return sum;
    }
}



