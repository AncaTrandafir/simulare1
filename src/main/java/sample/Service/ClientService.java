package sample.Service;

import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.Client.Client;
import sample.Repository.JDBCClientRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ClientService {

    private JDBCClientRepository repository;
    private Stack<UndoRedoOperation<Integer, Client>> undoableOperations = new Stack<>();
    private Stack<UndoRedoOperation<Integer, Client>> redoableOperations = new Stack<>();

    /**
     * Constructs a service.
     *
     * @param repository the backing repository.
     */
    public ClientService(JDBCClientRepository repository) {
        this.repository = repository;
    }


    /**
     * Adds a reservation with the given fields or updates an existing reservation.
     *
     * @param client - to be added.
         */
    public void add(Client client) {
        try {
            repository.save(client);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Gets a list of all reservations.
     *
     * @return a list of all invoices.
     */
    public Set<Client> getAll() {
        Iterable<Client> clients = repository.getAll();
        return StreamSupport.stream(clients.spliterator(), false).collect(Collectors.toSet());
    }

    /**
     * Gets an id whose object to remove.
     */
    public void remove(int id){
        repository.remove(id);
    }


    public Optional<Client> findById(int id) {
        return repository.findById(id);
    }
    
    public void update(Client client) throws CustomAccommodationException {
        repository.update(client);
    }


    public void undo() throws Exception {
        if (!undoableOperations.empty()) {
            UndoRedoOperation<Integer, Client> lastOperation = undoableOperations.pop();
            lastOperation.doUndo();
            redoableOperations.add(lastOperation);

        }
    }

    public void redo() throws Exception {
        if (!redoableOperations.empty()) {
            UndoRedoOperation<Integer, Client> lastOperation = redoableOperations.pop();
            lastOperation.doRedo();
            undoableOperations.add(lastOperation);
        }
    }

    public void clear() {
        List<Client> all = repository.getAll();
        for (Client c : all) {
            repository.remove(c.getId());
        }
        undoableOperations.push(new ClearOperation<>(repository, all));
        redoableOperations.clear();
    }

}



