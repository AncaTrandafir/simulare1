package sample.Domain.Client;
import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.IValidator;
import sample.UI.AlertBox;

public class ClientValidator implements IValidator<Client> {

    // Class LocalDate cannot be instantiated, we get current date directly with class name (static method): LocalDate.now()

    /**
     * Validates a check-in and check-out dates.
     * @param client the reservation subject to validation.
     * @throws CustomAccommodationException if the date is not in the correct format.
     */
    @Override
    public void validate(Client client) {

        if ( !client.getName().matches("[a-zA-Z][a-zA-Z ]+[a-zA-Z]$")) {
            String message = "Name must contain only letters.";
            AlertBox.showValidationError(message);
            throw new RuntimeException(message);
        }

    }
}
