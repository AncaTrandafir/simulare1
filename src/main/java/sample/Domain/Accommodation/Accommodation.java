package sample.Domain.Accommodation;

import sample.Domain.Entity;

import java.time.LocalDate;

public class Accommodation extends Entity<Integer> {

    private String client;
    private String company;
    private LocalDate checkIn;
    private LocalDate checkOut;
    private double roomRate;
    private String priceType;
    private String roomType;
    private double balance;
    private double restaurant;

    public Accommodation(){

    }

    // constructor without balance and restaurant that are automatically computed
    public Accommodation(String client, String company, LocalDate checkIn, LocalDate checkOut, double roomRate, String priceType, String roomType) {
        this.client = client;
        this.company = company;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.roomRate = roomRate;
        this.priceType = priceType;
        this.roomType = roomType;
    }

    // constrcutor with balance and restaurant atttributes
    public Accommodation(String client, String company, LocalDate checkIn, LocalDate checkOut, double roomRate, String priceType, String roomType, double balance, double restaurant) {
        this.client = client;
        this.company = company;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.roomRate = roomRate;
        this.priceType = priceType;
        this.roomType = roomType;
        this.balance = balance;
        this.restaurant = restaurant;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public LocalDate getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(LocalDate checkIn) {
        this.checkIn = checkIn;
    }

    public LocalDate getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDate checkOut) {
        this.checkOut = checkOut;
    }

    public double getRoomRate() {
        return roomRate;
    }

    public void setRoomRate(double roomRate) {
        this.roomRate = roomRate;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public double getBalance() {
        return balance;
    }

    public double getRestaurant() {
        return restaurant;
    }


    @Override
    public String toString() {
        return "Reservation{" + super.toString() +
                "client='" + client + '\'' +
                "company='" + company + '\'' +
                "checkIn='" + checkIn + '\'' +
                ", checkOut='" + checkOut + '\'' +
                ", roomRate=" + roomRate +
                ", priceType='" + priceType + '\'' +
                ", roomType='" + roomType + '\'' +
                ", balance='" + balance + '\'' +
                ", restaurant='" + restaurant + '\'' +
                '}';
    }
}
