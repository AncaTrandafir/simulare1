package sample.Domain.Accommodation;

import sample.Domain.IValidator;
import sample.UI.AlertBox;

import java.time.LocalDate;

public class AccommodationValidator implements IValidator<Accommodation> {

    // Class LocalDate cannot be instantiated, we get current date directly with class name (static method): LocalDate.now()

    /**
     * Validates a check-in and check-out dates.
     * @param accommodation the reservation subject to validation.
     * @throws CustomAccommodationException if the date is not in the correct format.
     */
    @Override
    public void validate(Accommodation accommodation) {

        if (accommodation.getCheckIn().isAfter(accommodation.getCheckOut())) {
            String message = "Check-in date cannot be after check-out";
            AlertBox.showValidationError(message);
            throw new RuntimeException(message);
        }

        if (accommodation.getCheckIn().isAfter(LocalDate.now())) {
            String message = "Check-in date of a past accommodation cannot be in the future.";
            AlertBox.showValidationError(message);
            throw new RuntimeException(message);
        }

        if (accommodation.getCheckOut().isAfter(LocalDate.now())) {
            String message = "Check-out date of a past accommodation cannot be in the future.";
            AlertBox.showValidationError(message);
            throw new RuntimeException(message);
        }

    }
}
