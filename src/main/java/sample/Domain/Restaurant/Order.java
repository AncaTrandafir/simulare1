package sample.Domain.Restaurant;

import sample.Domain.Entity;

import java.time.LocalDate;
import java.util.Date;

public class Order extends Entity<Integer> {

   private int idAccommodation;
   private LocalDate orderDate;
   private double orderValue;


    public Order(){

    }

    public Order(int idAccommodation, LocalDate orderDate, double orderValue) {
        this.idAccommodation = idAccommodation;
        this.orderDate = orderDate;
        this.orderValue = orderValue;
    }

    public int getIdAccommodation() {
        return idAccommodation;
    }

    public void setIdAccommodation(int idAccommodation) {
        this.idAccommodation = idAccommodation;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public double getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(double orderValue) {
        this.orderValue = orderValue;
    }

    @Override
    public String toString() {
        return "Order{"  + super.toString() +
                "idAccommodation=" + idAccommodation +
                ", orderDate=" + orderDate +
                ", orderValue=" + orderValue +
                '}';
    }
}


