package sample.Domain.Restaurant;

import sample.DBConfig.ConnectToDB;
import sample.Domain.Accommodation.Accommodation;
import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.IValidator;
import sample.Service.AccommodationService;
import sample.UI.AlertBox;

import java.io.IOException;
import java.time.LocalDate;

public class OrderValidator implements IValidator<Order> {
    private Accommodation accommodation;
    private AccommodationService accommodationService;


    // Class LocalDate cannot be instantiated, we get current date directly with class name (static method): LocalDate.now()

    /**
     * Validates orderDate between checkin and checkout
     *
     * @param order the reservation subject to validation.
     * @throws CustomAccommodationException if the date is not betwwn those dates
     */
    @Override
    public void validate(Order order) {

//        // iterate through accommodations and find the one accommodation.getId = order.getIdAccommodation
//        try {
//            ConnectToDB conn = new ConnectToDB();
//            conn.connectToDB();
//            accommodationService = conn.getAccommodationService();
//
//            while (accommodation.getId() == order.getIdAccommodation()) {
//                if (accommodation.getCheckIn().isAfter(order.getOrderDate()) || accommodation.getCheckOut().isBefore(order.getOrderDate())) {
//                    String message = "Order date must be between check-in and check-out dates.";
//                    AlertBox.showValidationError(message);
//                    throw new RuntimeException(message);
//                }
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//
//        ConnectToDB conn = new ConnectToDB();
//        conn.connectToDB();
//        companyService = conn.getCompanyService();
//        companies.addAll(companyService.getAll());
//        // Retrieve company by name
//        for (Company c : companies) {
//            if (c.getName().toLowerCase().equals(txtCompany.getText().toLowerCase())) {
//                idCompany = c.getId();
//            }
//        }
   }
}
