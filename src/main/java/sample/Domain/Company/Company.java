package sample.Domain.Company;

import sample.Domain.Entity;

public class Company extends Entity<Integer> {

    private String name;
    private String cif;
    private String regCom;
    private String country;
    private String city;
    private String address;

    public Company(){

    }

    public Company(String name, String cif, String regCom, String country, String city, String address) {
        this.name = name;
        this.cif = cif;
        this.regCom = regCom;
        this.country = country;
        this.city = city;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getRegCom() {
        return regCom;
    }

    public void setRegCom(String regCom) {
        this.regCom = regCom;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Company{" + super.toString() +
                "name='" + name + '\'' +
                ", cif='" + cif + '\'' +
                ", regCom='" + regCom + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
