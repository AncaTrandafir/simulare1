package sample.Domain.Company;
import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.Client.Client;
import sample.Domain.IValidator;
import sample.UI.AlertBox;

public class CompanyValidator implements IValidator<Company> {

    // Class LocalDate cannot be instantiated, we get current date directly with class name (static method): LocalDate.now()

    /**
     * Validates a check-in and check-out dates.
     * @param company the reservation subject to validation.
     * @throws CustomAccommodationException if the date is not in the correct format.
     */
    @Override
    public void validate(Company company) throws CustomAccommodationException {

        if ( (company.getName().length() < 3) || (company.getCif().length() < 3) || (company.getRegCom().length() < 3) || (company.getCity().length() < 3) || (company.getAddress().length() < 3)) {
            String message = "Incorrect numeber of letter.";
            AlertBox.showValidationError(message);
            throw new RuntimeException(message);
        }
    }
}
