//package sample.Repository;
//
//import com.google.gson.Gson;
//import sample.Domain.ReservationDateFormatException;
//import sample.Domain.Entity;
//import sample.Domain.IValidator;
//import sample.Domain.Reservation;
//
//import java.io.*;
//import java.lang.reflect.Type;
//import java.util.*;
//
//public class JsonFileRepository extends RepositoryImpl<Long, Reservation> {
//    private String filename;
//
//    /**
//     * Constructs a file repository.
//     * @param filename the filename used to store objects.
//     */
//    public JsonFileRepository(IValidator<T> validator, String filename, Type type) {
//        this.validator = validator;
//        this.filename = filename;
//    }
//
//    private void loadFromFile() {
//        storage.clear();
//        Gson gson = new Gson();
//        try (FileReader in = new FileReader(filename)) {
//            try (BufferedReader bufferedReader = new BufferedReader(in)) {
//           T[] entities = gson.fromJson(bufferedReader, type);
//                for (T entity : entities) {
//                    storage.put(entity.getId(), entity);
//                }
//            }
//        } catch (Exception ex) {
//
//        }
//    }
//
//    private void writeToFile() throws ReservationDateFormatException {
//        Gson gson = new Gson();
//        try (FileWriter out = new FileWriter(filename)) {
//            try (BufferedWriter bufferedWriter = new BufferedWriter(out)) {
//                bufferedWriter.write(gson.toJson(storage.values())); }
//
//        } catch (Exception ex) {
//
//        }
//    }
//
//
//    /**
//     * Gets an entity with a given id.
//     * @param id the given id.
//     * @return the entity with the given id.
//     */
//    @Override
//    public T findById(int id) {
//        loadFromFile();
//        return storage.get(id);
//    }
//
//
//    /**
//     * Adds or replaces an entity.
//     * @param entity the entity to add or replace based on its id.
//     */
//    @Override
//    public void upsert(T entity) throws ReservationDateFormatException {
//        loadFromFile();
//        validator.validate(entity);
//        storage.put(entity.getId(), entity);
//        writeToFile();
//    }
//
//
//    /**
//     * Removes an entity with the given id.
//     * @param id the given id.
//     * @throws RepositoryException if there is no entity with the given id.
//     */
//    @Override
//    public void remove(int id) {
//        loadFromFile();
//        if (!storage.containsKey(id)) {
//            throw new RuntimeException("There is no entity with the given id to remove.");
//        }
//
//        storage.remove(id);
//        writeToFile();
//    }
//
//    /**
//     * Gets a list of all entities.
//     * @return the list with all the entities.
//     */
//    @Override
//    public List<T> getAll() {
//        loadFromFile();
//        return new ArrayList<>(storage.values());
//    }
//}
