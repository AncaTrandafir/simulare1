package sample.Repository;

import sample.Domain.Entity;
import sample.Domain.Accommodation.CustomAccommodationException;


import java.util.Optional;

// Optional = may or may not contain a non-null value.
// Iterable interface =  to iterate over the elements.

public interface IRepository<ID, T extends Entity<ID>> {

    /**
     * Saves the given entity.
     *
     * @param entity must not be null.
     * @return a {@code Reservation} - null if the entity was saved otherwise (e.g. id already exists) returns the entity.
     * @throws IllegalArgumentException if the given entity is null.
     */
    Optional<T> save(T entity) throws CustomAccommodationException;


    /**
     * Gets an entity with a given id.
     * @param id the given id.
     * @return the entity with the given id.
     * @throws IllegalArgumentException if the given id is null.
     */
    Optional<T> findById(ID id) throws CustomAccommodationException;


    /**
     * Adds or replaces an entity.
     * @param entity the entity to add or replace based on its id.
     */
    Optional<T> update(T entity) throws CustomAccommodationException;


    /**
     * Removes an entity with the given id.
     * @param id the given id.
     */
    Optional<T> remove(ID id);


    /**
     * Gets a list of all entities.
     * @return the list with all the entities.
     */
    Iterable<T> getAll();
}