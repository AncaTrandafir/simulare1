package sample.Repository;

import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.Company.Company;
import sample.Domain.Company.CompanyValidator;
import sample.Domain.IValidator;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JDBCCompanyRepository extends RepositoryImpl<Integer, Company> {
    private String url;
    private String user;
    private String password;
    private CompanyValidator validator;

    public JDBCCompanyRepository(String url, String user, String password, CompanyValidator validator) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.validator = validator;
    }


    public List<Company> getAll() {
        List<Company> companies = new ArrayList<>();

        String sql = "select idcompany, companyname, cif, regcom, country, city, address from companies;";


        // Nu merge cu clasa proprie pt ca nu este closeable ???
//        try (
//        DBConfig connection = new DBConfig();
//        Connection conn = connection.getConnection();
//        ResultSet rs = conn.createStatement().executeQuery(sql)) {

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                int id = rs.getInt("idcompany");
                String name = rs.getString("companyname");
                String cif = rs.getString("cif");
                String regCom = rs.getString("regCom");
                String country = rs.getString("country");
                String city = rs.getString("city");
                String address = rs.getString("address");

                Company company = new Company(name, cif, regCom, country, city, address);
                company.setId(id);
                companies.add(company);
            }

//            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companies;
    }

    @Override
    public Optional<Company> save(Company company) throws CustomAccommodationException {
        validator.validate(company);
        Optional<Company> optional = Optional.ofNullable(company);
        String sql = "insert into companies (companyname, cif, regcom, country, city, address) values (?, ?, ?, ?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, company.getName());
            ps.setString(2, company.getCif());
            ps.setString(3, company.getRegCom());
            ps.setString(4, company.getCountry());
            ps.setString(5, company.getCity());
            ps.setString(6, company.getAddress());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return optional;
    }

    public Optional<Company> update(Company company) throws CustomAccommodationException {
        validator.validate(company);
        Optional<Company> optional = Optional.ofNullable(company);
        String sql = "update companies set companyname=?, cif=?, regcom=?, country=?, city=?, address=? where idcompany=?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, company.getName());
            ps.setString(2, company.getCif());
            ps.setString(3, company.getRegCom());
            ps.setString(4, company.getCountry());
            ps.setString(5, company.getCity());
            ps.setString(6, company.getAddress());
            ps.setInt(7, company.getId());


            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return optional;
    }

    @Override
    public Optional<Company> remove(Integer id) {
        String sql = "delete from companies where idcompany=?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Company> findById(Integer idValue){
        Company company = new Company();

        try {
            Connection connection = DriverManager.getConnection(url, user, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM companies WHERE idcompany = '" + idValue + "'");

            ResultSet result = statement.executeQuery();

            //This array has all data in single record.
            ArrayList<String> array = new ArrayList<String>();
            result.next();

            array.add(result.getString("idcompany"));
            array.add(result.getString("companyname"));
            array.add(result.getString("cif"));
            array.add(result.getString("regcom"));
            array.add(result.getString("country"));
            array.add(result.getString("city"));
            array.add(result.getString("address"));

            int id = Integer.parseInt(array.get(0));
            String name = array.get(1);
            String cif = array.get(2);
            String regCom = array.get(3);
            String country = array.get(4);
            String city = array.get(5);
            String address = array.get(6);

            company.setId(id);
            company.setName(name);
            company.setCif(cif);
            company.setRegCom(regCom);
            company.setCountry(country);
            company.setCity(city);
            company.setAddress(address);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(company);
    }


}
