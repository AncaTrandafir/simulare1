package sample.Repository;

import com.sun.org.apache.xpath.internal.operations.Or;
import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.Company.Company;
import sample.Domain.Company.CompanyValidator;
import sample.Domain.Restaurant.Order;
import sample.Domain.Restaurant.OrderValidator;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JDBCOrderRepository extends RepositoryImpl<Integer, Order> {
    private String url;
    private String user;
    private String password;
    private OrderValidator validator;

    public JDBCOrderRepository(String url, String user, String password, OrderValidator validator) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.validator = validator;
    }

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public List<Order> getAll() {
        List<Order> orders = new ArrayList<>();

        String sql = "select idorder, idaccommodation, to_char(orderdate, 'dd.MM.yyyy') as alias, ordervalue from rest_orders;";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                int id = rs.getInt("idorder");
                int idAccommodation = rs.getInt("idaccommodation");
                LocalDate dateOrder = LocalDate.parse(rs.getString("alias"), DateTimeFormatter.ofPattern("dd.MM.uuuu"));
                double valueOrder = rs.getDouble("ordervalue");

                Order order = new Order(idAccommodation, dateOrder, valueOrder);
                order.setId(id);
                orders.add(order);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public Optional<Order> save(Order order) throws CustomAccommodationException {
        validator.validate(order);
        Optional<Order> optional = Optional.ofNullable(order);
        String sql = "insert into rest_orders(idaccommodation, orderdate, ordervalue) values (?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, order.getIdAccommodation());
            ps.setDate(2, Date.valueOf(simpleDateFormat.format(Date.valueOf(order.getOrderDate()))));
            ps.setDouble(3, order.getOrderValue());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return optional;
    }

    public Optional<Order> update(Order order) throws CustomAccommodationException {
        validator.validate(order);
        Optional<Order> optional = Optional.ofNullable(order);
        String sql = "update rest_orders set idaccommodation=?, orderdate=?, ordervalue=? where idorder=?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, order.getIdAccommodation());
            ps.setDate(2, Date.valueOf(simpleDateFormat.format(Date.valueOf(order.getOrderDate()))));
            ps.setDouble(3, order.getOrderValue());
            ps.setInt(4, order.getId());


            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return optional;
    }

    @Override
    public Optional<Order> remove(Integer id) {
        String sql = "delete from rest_orders where idorder=?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Order> findById(Integer idValue){
        Order order = new Order();

        try {
            Connection connection = DriverManager.getConnection(url, user, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM rest_orders WHERE idorder = '" + idValue + "'");

            ResultSet result = statement.executeQuery();

            //This array has all data in single record.
            ArrayList<String> array = new ArrayList<String>();
            result.next();

            array.add(result.getString("idaccommodation"));
            array.add(result.getString("orderdate"));
            array.add(result.getString("ordervalue"));

            int id = Integer.parseInt(array.get(0));
            int idaccommodation = Integer.parseInt(array.get(1));
            LocalDate orderDate = LocalDate.parse(array.get(2), DateTimeFormatter.ofPattern("dd.MM.uuuu"));
            double orderValue = Double.parseDouble(array.get(3));

            order.setId(id);
            order.setIdAccommodation(idaccommodation);
            order.setOrderDate(orderDate);
            order.setOrderValue(orderValue);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(order);
    }


    public List<Order> getordersBetweenDates(LocalDate startDate, LocalDate endDate) throws CustomAccommodationException {
        List<Order> orders = new ArrayList<>();
        String sql = "select * from rest_orders where orderdate between \'" + startDate + "\' and " + endDate +";";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                int id = rs.getInt("idorder");
                int idAccommodation = rs.getInt("idaccommodation");
                LocalDate orderDate = LocalDate.parse(rs.getString("orderdate"));
                double orderVAlue = rs.getDouble("ordervalue");

                Order order = new Order(idAccommodation, orderDate, orderVAlue);
                order.setId(id);
                orders.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }


}
