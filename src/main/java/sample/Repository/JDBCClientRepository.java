package sample.Repository;

import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Domain.Client.Client;
import sample.Domain.Client.ClientValidator;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JDBCClientRepository extends RepositoryImpl<Integer, Client> {
    private String url;
    private String user;
    private String password;
    private ClientValidator validator;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public JDBCClientRepository(String url, String user, String password, ClientValidator validator) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.validator = validator;
    }


    public List<Client> getAll() {
        List<Client> clients = new ArrayList<>();

        // Cannot use Select * because i have to specifically format checkin and checkout in dd.MM.uuuu with to_char(date, format)
        // to_char() changes the name of the column so we use alias to reference it
        String sql = "select idclient, clientname, gender, to_char(birthday, 'dd.MM.yyyy') as alias, country, city, address from clients;";


        // Nu merge cu clasa proprie pt ca nu este closeable ???
//        try (
//        DBConfig connection = new DBConfig();
//        Connection conn = connection.getConnection();
//        ResultSet rs = conn.createStatement().executeQuery(sql)) {

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                int id = rs.getInt("idclient");
                String name = rs.getString("clientname");
                String gender = rs.getString("gender");
                LocalDate birthday = LocalDate.parse(rs.getString("alias"), DateTimeFormatter.ofPattern("dd.MM.uuuu"));
                String country = rs.getString("country");
                String city = rs.getString("city");
                String address = rs.getString("address");

                Client client = new Client(name, gender, birthday, country, city, address);
                client.setId(id);
                clients.add(client);
            }

//            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clients;
    }

    @Override
    public Optional<Client> save(Client client) throws CustomAccommodationException {
        Optional<Client> optional = Optional.ofNullable(client);
        validator.validate(client);
        String sql = "insert into clients (clientname, gender, birthday, country, city, address) values (?, ?, ?, ?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, client.getName());
            ps.setString(2, client.getGender());

            // SimpleDateFormat.format(pattern) to transform from reservation date pattern dd.MM.uuuu to sql date pattern uuuu-MM-dd
            // reservation.getCheckIn() retrieves LocalDate type, i need Date SQL type, so i wrap it in Date.valueOf()

            ps.setDate(3, Date.valueOf(simpleDateFormat.format(Date.valueOf(client.getBirthday()))));
            ps.setString(4, client.getCountry());
            ps.setString(5, client.getCity());
            ps.setString(6, client.getAddress());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return optional;
    }

    public Optional<Client> update(Client client) throws CustomAccommodationException {
        validator.validate(client);
        Optional<Client> optional = Optional.ofNullable(client);
        String sql = "update clients set clientname=?, gender=?, birthday=?, country=?, city=?, address=? where idclient=?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, client.getName());
            ps.setString(2, client.getGender());

            // SimpleDateFormat.format(pattern) to transform from reservation date pattern dd.MM.uuuu to sql date pattern uuuu-MM-dd
            ps.setDate(3, Date.valueOf(simpleDateFormat.format(Date.valueOf(client.getBirthday()))));

            ps.setString(4, client.getCountry());
            ps.setString(5, client.getCity());
            ps.setString(6, client.getAddress());
            ps.setInt(7, client.getId());


            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return optional;
    }

    @Override
    public Optional<Client> remove(Integer id) {
        String sql = "delete from clients where idclient=?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Client> findById(Integer idValue){
        Client client = new Client();

        try {
            Connection connection = DriverManager.getConnection(url, user, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM clients WHERE idclient = '" + idValue + "'");

            ResultSet result = statement.executeQuery();

            //This array has all data in single record.
            ArrayList<String> array = new ArrayList<String>();
            result.next();

            array.add(result.getString("idclient"));
            array.add(result.getString("clientname"));
            array.add(result.getString("gender"));
            array.add(result.getString("birthday"));
            array.add(result.getString("country"));
            array.add(result.getString("city"));
            array.add(result.getString("address"));

            int id = Integer.parseInt(array.get(0));
            String name = array.get(1);
            String gender = array.get(2);
            LocalDate birthday = LocalDate.parse(array.get(3), DateTimeFormatter.ofPattern("dd.MM.uuuu"));
            String country = array.get(4);
            String city = array.get(5);
            String address = array.get(6);

            client.setId(id);
            client.setName(name);
            client.setGender(gender);
            client.setBirthday(birthday);
            client.setCountry(country);
            client.setCity(city);
            client.setAddress(address);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(client);
    }


}
