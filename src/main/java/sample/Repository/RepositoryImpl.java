package sample.Repository;

import sample.Domain.Entity;
import sample.Domain.IValidator;
import sample.Domain.Accommodation.CustomAccommodationException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class RepositoryImpl<ID, T extends Entity<ID>> implements IRepository<ID, T> {
    private Map<ID, T> entities;
    private IValidator<T> validator;

    /**
     * Constructs an InMemory repository.
     * @param validator the validator.
     * dictionary <id type, id entity>
     */
    public RepositoryImpl(IValidator<T> validator) {
        this.validator = validator;
        this.entities = new HashMap<>();
    }

    public RepositoryImpl() {
    }
    // entities asociaza id cu obiectul, cand sterg id de fapt sterg obiectul entities care are asociat id


    /**
     * Returns a reservation with the given id.
     * @param id the id.
     * @throws RuntimeException if there's no reservation with the given id.
     */

    @Override
    public Optional<T> findById(ID id) throws CustomAccommodationException {
        if (id == null) {
            throw new IllegalArgumentException("Id must not be null");
        }
        return Optional.ofNullable(entities.get(id));
    }

    // Optional.ofNullable = used to get an instance of this Optional class with the specified value of the specified type: Optional[entitate]
    // If the specified value is null, it returns an empty instance of the Optional class: Optional.empty

    /**
     * Returns all accommodations.
     */
    @Override
    public Iterable<T> getAll() {
        Set<T> allEntities = entities.entrySet().stream().map(entry -> entry.getValue()).collect(Collectors.toSet());
        return allEntities;
    }


    @Override
    public Optional<T> save(T entity) throws CustomAccommodationException {

        if (entity == null) {
            throw new IllegalArgumentException("Id must not be null");
        }
        validator.validate(entity);
        return Optional.ofNullable(entities.putIfAbsent(entity.getId(), entity));
    }



    /**
     * Removes an accommodation with the given id.
     * @param id the id.
     * @throws RuntimeException if there's no movie with the given id.
     */
    @Override
    public Optional<T> remove(ID id) {
        return Optional.ofNullable(entities.remove(id));
    }


    /**
     * Adds or updates a movie if it already exists.
     * @param entity the movie to add or update.
     */
    @Override
    public Optional<T> update(T entity) throws CustomAccommodationException {
        if (entity == null) {
            throw new IllegalArgumentException("Entity must not be null");
        }
        validator.validate(entity);
        return Optional.ofNullable(entities.computeIfPresent(entity.getId(), (k, v) -> entity));
    }


}
