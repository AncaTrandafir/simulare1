package sample.Repository;

import sample.DBConfig.ConnectToDB;
import sample.Domain.Accommodation.Accommodation;
import sample.Domain.IValidator;
import sample.Domain.Accommodation.CustomAccommodationException;
import sample.Service.AccommodationService;
import sample.Service.ClientService;
import sample.Service.CompanyService;
import sample.UI.AccommodationAddController;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class JDBCAccommodationRepository extends RepositoryImpl<Integer, Accommodation> {
    private String url;
    private String user;
    private String password;
    private IValidator validator;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public JDBCAccommodationRepository(String url, String user, String password, IValidator<Accommodation> validator) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.validator = validator;
    }

    private AccommodationService accommodationService;
    private ClientService clientService;
    private CompanyService companyService;

    ///////////// LUCRAM CU VIEW PT A VIZUALIZA NUMECLIENT SI NUMECOMPANIE DIN TABELELE CELELALTE
    ///////////// TABELUL BUN E ACCOMMODATIONS1

    // getAll are
    public List<Accommodation> getAll() {
        List<Accommodation> accommodations = new ArrayList<>();

        // Cannot use Select * because i have to specifically format checkin and checkout in dd.MM.uuuu with to_char(date, format)
        // to_char() changes the name of the column so we use alias to reference it
        String sql = "select idaccommodation, clientname, companyname, to_char(checkin, 'dd.MM.yyyy') as alias1, to_char(checkout, 'dd.MM.yyyy') as alias2, roomRate, pricetype, roomtype, balance, restaurant from viewaccommodations;";

        // Nu merge cu clasa proprie pt ca nu este closeable ???
//        try (
//        DBConfig connection = new DBConfig();
//        Connection conn = connection.getConnection();
//        ResultSet rs = conn.createStatement().executeQuery(sql)) {

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                int id = rs.getInt("idaccommodation");
                String client = rs.getString("clientname");
                String company = rs.getString("companyname");
                LocalDate checkIn =  LocalDate.parse(rs.getString("alias1"), DateTimeFormatter.ofPattern("dd.MM.uuuu"));
                LocalDate checkOut =  LocalDate.parse(rs.getString("alias2"), DateTimeFormatter.ofPattern("dd.MM.uuuu"));
                double roomRate = rs.getDouble("roomrate");
                String priceType = rs.getString("pricetype");
                String roomType = rs.getString("roomtype");
                double balance = rs.getDouble("balance");
                double restaurant = rs.getDouble("restaurant");

                Accommodation accommodation = new Accommodation(client, company, checkIn, checkOut, roomRate, priceType, roomType, balance, restaurant);
                accommodation.setId(id);
                accommodations.add(accommodation);
            }

//            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return accommodations;
    }




    // We cannot insert directly into the view, so we create a procedure and trigger. (vezi sql) --> complicated
    // Or we insert first in accommodations1 table which is in accommodationaddcontroller beacuse we need to get txt from textfield of client and company
    // then we see in tha tableview all updated because it's a view on accommodations1 table
    // so this below is empty

    @Override
    public Optional<Accommodation> save(Accommodation accommodation) throws CustomAccommodationException {
        Optional<Accommodation> optional = Optional.ofNullable(accommodation);
        validator.validate(accommodation);
        return optional;
    }


    public Optional<Accommodation> update(Accommodation accommodation) throws CustomAccommodationException {
        Optional<Accommodation> optional = Optional.ofNullable(accommodation);
        validator.validate(accommodation);
        String sql = "update viewaccommodations set clientname=?, companyname=?, checkIn=?, checkOut=?, roomRate=?, priceType=?, roomType=?, where id=?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, accommodation.getClient());
            ps.setString(2, accommodation.getCompany());

            // SimpleDateFormat.format(pattern) to transform from reservation date pattern dd.MM.uuuu to sql date pattern uuuu-MM-dd
            ps.setDate(3, Date.valueOf(simpleDateFormat.format(Date.valueOf(accommodation.getCheckIn()))));
            ps.setDate(4, Date.valueOf(simpleDateFormat.format(Date.valueOf(accommodation.getCheckOut()))));

            ps.setDouble(5, accommodation.getRoomRate());
            ps.setString(6, accommodation.getPriceType());
            ps.setString(7, accommodation.getRoomType());
            ps.setDouble(8, accommodation.getRoomRate());
            ps.setInt(9, accommodation.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return optional;
    }

    // we cannot delete from the view, but we can delete from accommodations table, and we reference to it with idaccommodation only
    @Override
    public Optional<Accommodation> remove(Integer id) {
        String sql = "delete from accommodations1 where idaccommodation=?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Accommodation> findById(Integer idValue) throws CustomAccommodationException {
        Accommodation accommodation = new Accommodation();

        try {
            Connection connection = DriverManager.getConnection(url, user, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM viewaccommodations WHERE idaccommodation = '" + idValue + "'");

            ResultSet result = statement.executeQuery();

            //This array has all data in single record.
            ArrayList<String> array = new ArrayList<String>();
            result.next();

            array.add(result.getString("idaccommodation"));
            array.add(result.getString("clientname"));
            array.add(result.getString("companyname"));
            array.add(result.getString("checkIn"));
            array.add(result.getString("checkOut"));
            array.add(result.getString("roomrate"));
            array.add(result.getString("priceType"));
            array.add(result.getString("roomType"));

            int id = Integer.parseInt(array.get(0));
            String client = array.get(1);
            String company = array.get(2);
            LocalDate checkIn = LocalDate.parse(array.get(3), DateTimeFormatter.ofPattern("dd.MM.uuuu"));
            LocalDate checkOut = LocalDate.parse(array.get(4), DateTimeFormatter.ofPattern("dd.MM.uuuu"));
            double roomRate = Double.parseDouble(array.get(5));
            String priceType = array.get(6);
            String roomType = array.get(7);

            accommodation.setId(id);
            accommodation.setClient(client);
            accommodation.setClient(company);
            accommodation.setCheckIn(checkIn);
            accommodation.setCheckOut(checkOut);
            accommodation.setRoomRate(roomRate);
            accommodation.setPriceType(priceType);
            accommodation.setRoomType(roomType);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(accommodation);
    }

    public List<Accommodation> getReservationsBetweenDates(LocalDate startDate, LocalDate endDate) throws CustomAccommodationException {
        List<Accommodation> accommodations = new ArrayList<>();
        String sql = "select * from viewaccommodations where checkin between \'" + startDate + "\' and \'" + endDate + "\' and checkout between \'" + startDate + "\' and \'" + endDate + "\';";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                int id = rs.getInt("idaccommodation");
                String client = rs.getString("clientname");
                String company = rs.getString("companyname");
                LocalDate checkIn = LocalDate.parse(rs.getString("checkIn"));
                LocalDate checkOut = LocalDate.parse(rs.getString("checkOut"));
                double roomRate = rs.getDouble("roomRate");
                String priceType = rs.getString("priceType");
                String roomType = rs.getString("roomType");

                Accommodation accommodation = new Accommodation(client, company, checkIn, checkOut, roomRate, priceType, roomType);
                accommodation.setId(id);
                accommodations.add(accommodation);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return accommodations;
    }
}
