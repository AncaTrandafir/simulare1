package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.DBConfig.DBConfig;
import java.io.IOException;

public class Main extends Application {

    public Main() throws IOException {
    }


    private DBConfig dbConfig = new DBConfig();

    @Override
    public void start(Stage primaryStage) throws Exception{

        dbConfig.getConnection();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample.fxml"));
        Parent root = fxmlLoader.load();

        primaryStage.setTitle("Reservation manager");
        primaryStage.setScene(new Scene(root, 655, 523));

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
