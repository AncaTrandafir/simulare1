package sample.DBConfig;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConfig {
    // Creates a database connection class
    protected Connection connection;

    DBProperties dbProperties = new DBProperties();

    // Connection string, includes host, port number, database name
    private final String URL = dbProperties.getURL();
    private final String USER = dbProperties.getUser();
    private final String PASSWORD = dbProperties.getPassword();

    public DBConfig() throws IOException {
    }

    public String getURL() {
        return URL;
    }

    public String getUSER() {
        return USER;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    // Connection method that returns connection instance
    public Connection getConnection() {

        try {
            // Create connection using driveManager, with 3 parameters: url, user, password
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Successful connection to database");
        } catch (SQLException ex) {
            Logger.getLogger(DBConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }

    public void close() throws SQLException {
        connection.close();
    }


}
