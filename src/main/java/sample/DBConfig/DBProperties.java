package sample.DBConfig;

import sample.Main;

import java.io.*;
import java.sql.DriverManager;
import java.util.Properties;

public class DBProperties {

    private Properties prop;

    public DBProperties () throws IOException {

    }


    public void getFile() {
        try (InputStream input = getClass().getClassLoader().getResourceAsStream("db.properties")) {
            prop = new Properties();

            if (input == null) {
                System.out.println("Unable to find file db.properties");
                return;
            }

            // load a properties file
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    public String getDRIVER() {
        getFile();
        return prop.getProperty("db.driver");
    }

    public String getURL() {
        getFile();
        return prop.getProperty("db.url");
    }

    public String getUser() {
        getFile();
        return prop.getProperty("db.user");
    }

    public String getPassword() {
        getFile();
        return prop.getProperty("db.password");
    }

}


