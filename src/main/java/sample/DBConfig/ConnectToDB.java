package sample.DBConfig;

import sample.Domain.Accommodation.AccommodationValidator;
import sample.Domain.Client.ClientValidator;
import sample.Domain.Company.Company;
import sample.Domain.Company.CompanyValidator;
import sample.Domain.Restaurant.OrderValidator;
import sample.Repository.JDBCAccommodationRepository;
import sample.Repository.JDBCClientRepository;
import sample.Repository.JDBCCompanyRepository;
import sample.Repository.JDBCOrderRepository;
import sample.Service.AccommodationService;
import sample.Service.ClientService;
import sample.Service.CompanyService;
import sample.Service.OrderService;

import java.io.IOException;

public class ConnectToDB {

    private DBConfig conn;
    private AccommodationValidator accommodationValidator;
    private ClientValidator clientValidator;
    private CompanyValidator companyValidator;
    private OrderValidator orderValidator;

    private JDBCAccommodationRepository accommodationRepository;
    private AccommodationService accommodationService;
    private JDBCClientRepository clientRepository;
    private ClientService clientService;
    private JDBCCompanyRepository companyRepository;
    private CompanyService companyService;
    private JDBCOrderRepository orderRepository;
    private OrderService orderService;

    public DBConfig getConn() {
        return conn;
    }

    public AccommodationValidator getAccommodationValidator() {
        return accommodationValidator;
    }
    public ClientValidator getClientValidator() {
        return clientValidator;
    }
    public CompanyValidator getCompanyValidator() { return  companyValidator;}

    public JDBCAccommodationRepository getAccommodationRepository() {
        return accommodationRepository;
    }

    public AccommodationService getAccommodationService() {
        return accommodationService;
    }

    public JDBCClientRepository getClientRepository() {
        return clientRepository;
    }

    public void setClientRepository(JDBCClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public ClientService getClientService() {
        return clientService;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    public JDBCCompanyRepository getCompanyRepository() {
        return companyRepository;
    }

    public void setCompanyRepository(JDBCCompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public CompanyService getCompanyService() {
        return companyService;
    }

    public void setCompanyService(CompanyService companyService) {
        this.companyService = companyService;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void connectToDB() throws IOException {
        conn = new DBConfig();
        String url = conn.getURL();
        String user = conn.getUSER();
        String password = conn.getPASSWORD();

        AccommodationValidator accommodationValidator = new AccommodationValidator();
        accommodationRepository = new JDBCAccommodationRepository(url, user, password, accommodationValidator);
        accommodationService = new AccommodationService(accommodationRepository);

        ClientValidator clientValidator = new ClientValidator();
        clientRepository = new JDBCClientRepository(url, user, password, clientValidator);
        clientService = new ClientService(clientRepository);

        CompanyValidator companyValidator = new CompanyValidator();
        companyRepository = new JDBCCompanyRepository(url, user, password, companyValidator);
        companyService = new CompanyService(companyRepository);

        OrderValidator orderValidator = new OrderValidator();
        orderRepository = new JDBCOrderRepository(url, user, password, orderValidator);
        orderService = new OrderService(orderRepository);
    }

}
